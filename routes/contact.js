exports.contact = function(req, res){
    
  console.log("======= Request recieved =======");

  var models = {};
  models.contactform = require('../models/contactFormDbModel')(mongoose);
  
  var DB = models.contactform;
  var db = new DB();
  
  //If the form submit is a AJAX call
  //var object=req.body;
  //db.fname = object.fname;
  //db.lname = object.lname;
  //db.email = object.email;
  
  db.fname = req.param('fname');
  db.lname = req.param('lname');
  db.email = req.param('email');
  db.message = req.param('message');

  db.save(function(error){
    if (error) {
      throw error;
    }
    console.log("======= Saved =======");
  });

  
  res.render('thanks', { title: 'Thanks !' });

  //JSONP Call back
  //res.writeHead(200, {'Content-Type': 'json'});
  //res.end('loanData_cb(\'{"status": "OK"}\')');


};