$(document).ready(function(){
    $("#createLoan").click(function(e) {
        e.preventDefault(); // Prevents the page from refreshing
        //var $this = $(this); // `this` refers to the current form element
        
        var dataObj = new Object();
        
        var fname = $('#fname').val();
        var lname = $('#lname').val();
        var email = $('#email').val();
        var message = $('#message').val();
        
        dataObj.fname = fname;
        dataObj.lname = lname;
        dataObj.email = email;
        dataObj.message = message;
        
        $.ajax({
            type: "POST",
            
            url: '/contact',
            data: dataObj,
            dataType: "jsonp",
            jsonpCallback: "contact_cb",
            cache: false,
            success: function(data)
            {
                //location.href = "/thanks";
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log('error ' + textStatus + " " + errorThrown);
            }
        }); 
        //console.log(dataObj);
        
    });
});
