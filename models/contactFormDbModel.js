module.exports=function(mongoose){
    var collection = 'contact';
    var schm = mongoose.Schema;
    var objId = schm.ObjectId;
    
    var schema = new schm({
        id : objId,
        fname: String,
        lname: String,
        email: String,
        message: String
    });
    
    return mongoose.model(collection, schema);
}