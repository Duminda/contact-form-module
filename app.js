
/**
 * Module dependencies.
 */

var express = require('express')
  , routes = require('./routes')
  , user = require('./routes/user')
  , contact = require('./routes/contact')
  , http = require('http')
  , path = require('path');
    mongoose = require('mongoose');

var DB_URI;

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
  
  DB_URI = 'mongodb://localhost/contactFormDB';
  
  mongoose.connect(DB_URI);
}

app.get('/', routes.index);
app.get('/users', user.list);
app.post('/contact', contact.contact);

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
